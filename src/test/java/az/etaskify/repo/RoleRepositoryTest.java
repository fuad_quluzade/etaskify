package az.etaskify.repo;

import az.etaskify.dao.Role;
import az.etaskify.repository.RoleRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
public class RoleRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    @Test
    public void deleteRoleAndThenFindByNameIsNullOrNotTest(){
        Role role=roleRepository.findById(1l).get();
        roleRepository.delete(role);

        Role role1=null;

        Optional<Role> optionalRole=roleRepository.findByName("USER");

        if(optionalRole.isPresent())
            role1=optionalRole.get();

        Assertions.assertThat(role1).isNull();
    }
}
