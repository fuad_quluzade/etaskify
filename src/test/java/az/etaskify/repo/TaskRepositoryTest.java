package az.etaskify.repo;

import az.etaskify.dao.Task;
import az.etaskify.repository.TaskRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TaskRepositoryTest {
    @Autowired
    private TaskRepository taskRepository;

    @Test
    @Rollback(value = false)
    @Order(1)
    public void saveTaskTest(){

        Task task= Task.builder()
                .id(1L)
                .title("Tasks")
                .description("First task")
                .deadline(LocalDate.now())
                .build();
        taskRepository.save(task);

        Assertions.assertThat(task.getId()).isGreaterThan(0);
    }

    @Test
    @Rollback(value = false)
    @Order(2)
    public void getTaskTest(){
        Task task=taskRepository.findById(1L).get();
        Assertions.assertThat(task.getId()).isEqualTo(1L);
    }

    @Test
    @Rollback(value = false)
    @Order(3)
    public void getListTasksTest(){
        List<Task> task=taskRepository.findAll();
        Assertions.assertThat(task.size()).isGreaterThan(0);
    }

    @Test
    @Rollback(value = false)
    @Order(4)
    public void updateTaskTest(){
        Task task=taskRepository.findById(1L).get();
        task.setTitle("update title");
       Task taskUpdated= taskRepository.save(task);
        Assertions.assertThat(taskUpdated.getTitle()).isEqualTo("update title");
    }



}
