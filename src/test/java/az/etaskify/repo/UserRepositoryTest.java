package az.etaskify.repo;

import az.etaskify.dao.Customer;
import az.etaskify.dao.Role;
import az.etaskify.dao.User;
import az.etaskify.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;


@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Rollback(value = false)
    @Order(1)
    void itShouldBeCheckIfUserExistsByEmail() {
        String email = "quluzade@gmail.com";
        Customer customer = prepareCustomer();
        Role role= prepareRole();
        User user = User.builder()
                .username("f123456")
                .password("12345678")
                .name("Fuad")
                .surname("Quluzade")
                .email(email)
                .customer(customer)
                .role(role)
                .build();

        userRepository.save(user);

        boolean expected = userRepository.existsByEmail(email);

        Assertions.assertThat(expected).isTrue();

    }

    @Test
    @Rollback(value = false)
    @Order(2)
    void itShouldBeCheckIfUserExistsByUsername() {
        String username = "Fuad";
        Customer customer = prepareCustomer();
        Role role= prepareRole();
        User user = User.builder()
                .username(username)
                .password("12345678")
                .name("Fuad")
                .surname("Quluzade")
                .email("email@guluzade")
                .customer(customer)
                .role(role)
                .build();

        userRepository.save(user);

        boolean expected = userRepository.existsByUsername(username);

        Assertions.assertThat(expected).isTrue();

    }

    public Customer prepareCustomer(){
        return  Customer.builder()
                .id(1L)
                .name("LLC")
                .build();
    }

    public Role prepareRole(){
        return Role.builder()
                .id(1L)
                .name("USER")
                .build();
    }
}
