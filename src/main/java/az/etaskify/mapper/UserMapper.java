package az.etaskify.mapper;

import az.etaskify.dao.User;
import az.etaskify.model.dto.UserDto;
import az.etaskify.model.request.CreateUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class UserMapper {

    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "password", ignore = true)
    public abstract User userDtoToEntity(UserDto dto);

    public abstract UserDto userEntityToDto(User entity);

    @Mapping(target = "role", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "id", ignore = true)
    public abstract User requestToEntity(CreateUserRequest user);

}
