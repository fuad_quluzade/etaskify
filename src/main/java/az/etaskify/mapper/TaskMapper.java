package az.etaskify.mapper;

import az.etaskify.dao.Task;
import az.etaskify.model.dto.TaskDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class TaskMapper {

    public static final TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    public abstract Task taskDtoToEntity(TaskDto dto);

    public abstract TaskDto taskEntityToDto(Task entity);
}
