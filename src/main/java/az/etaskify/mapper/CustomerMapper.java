package az.etaskify.mapper;

import az.etaskify.dao.Customer;
import az.etaskify.model.dto.CustomerDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class CustomerMapper {

    public static final CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    public abstract CustomerDto customerEntityToDto(Customer entity);
}
