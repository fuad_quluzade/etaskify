package az.etaskify.controller;

import az.etaskify.model.request.LoginRequest;
import az.etaskify.model.request.SignupRequest;
import az.etaskify.model.response.JwtResponse;
import az.etaskify.model.response.MessageResponse;
import az.etaskify.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
@Api("Authentication controller")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/signin")
    @ApiOperation(value = "Login with username and password")
    public JwtResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
                                        @ApiParam(hidden = true) @NotNull HttpServletResponse response) {
        return authService.authenticateUser(loginRequest, response);

    }

    @PostMapping("/signup")
    @ApiOperation(value = "New SUPER_USER register for new customer")
    public MessageResponse registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return authService.registerUser(signUpRequest);
    }
}
