package az.etaskify.controller;

import az.etaskify.model.dto.TaskDto;
import az.etaskify.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/task")
@Api("Task Controller")
public class TaskController {

    private final TaskService taskService;

    @PostMapping("/create")
    @PreAuthorize("hasRole('SUPER_USER')")
    @ApiOperation(value = "Create task")
    public TaskDto createTask(@Valid @RequestBody TaskDto request) {
        return taskService.createTask(request);
    }

    @GetMapping("/{customerId}/{userId}")
    @PreAuthorize("hasRole('USER') or hasRole('SUPER_USER')")
    @ApiOperation(value = "Find task by id")
    public TaskDto getTaskById(@PathVariable Long customerId,
                               @PathVariable Long userId) {
        return taskService.getTaskByIdAndCustomer(customerId, userId);
    }

    @GetMapping("/delete/{customerId}/{userId}")
    @PreAuthorize("hasRole('SUPER_USER')")
    @ApiOperation(value = "Delete customer task by id")
    public void deleteTaskByCustomer(@PathVariable Long customerId,
                                         @PathVariable Long userId) {
        taskService.deleteTaskByCustomer(customerId, userId);
    }

    @GetMapping("/mark-done/{userId}")
    @PreAuthorize("hasRole('USER') or hasRole('SUPER_USER')")
    @ApiOperation(value = "Mark task is completed")
    public void setTaskCompleted(@PathVariable Long userId) {
        taskService.completedTask(userId);
    }

    @GetMapping("/unmark-done/{userId}")
    @PreAuthorize("hasRole('USER') or hasRole('SUPER_USER')")
    @ApiOperation(value = "Mark task is not completed")
    public void setTaskNotCompleted(@PathVariable Long userId) {
        taskService.notCompletedTask(userId);
    }

    @GetMapping("/{customerId}/unassigned")
    @PreAuthorize("hasRole('USER') or hasRole('SUPER_USER')")
    @ApiOperation(value = "Find all unassigned taska")
    public List<TaskDto> findFreeTasks(@PathVariable Long customerId) {
        return taskService.findFreeTasks(customerId);
    }

    @GetMapping("/{customerId}/tasks")
    @PreAuthorize("hasRole('USER') or hasRole('SUPER_USER')")
    @ApiOperation(value = "Find all tasks")
    public List<TaskDto> findAllTasks(@PathVariable Long customerId) {
        return taskService.findTasksByCustomer(customerId);
    }

}
