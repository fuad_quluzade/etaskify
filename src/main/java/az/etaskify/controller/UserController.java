package az.etaskify.controller;

import az.etaskify.model.dto.UserDto;
import az.etaskify.model.request.CreateUserRequest;
import az.etaskify.model.response.MessageResponse;
import az.etaskify.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/user")
@Api("Users Controller")
public class UserController {

    private final UserService userService;

    @GetMapping("/{customerId}/users")
    @PreAuthorize("hasRole('USER') or hasRole('SUPER_USER')")
    @ApiOperation(value = "Find all users")
    public List<UserDto> listUsers(@PathVariable Long customerId) {
        return userService.getUsersByCustomer(customerId);
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('SUPER_USER')")
    @ApiOperation(value = "Create new user(password:defaultPassword)")
    public MessageResponse createUser(@Valid @RequestBody CreateUserRequest request) {
        return userService.createUser(request);
    }

    @GetMapping("/{customerId}/{userId}")
    @PreAuthorize("hasRole('USER') or hasRole('SUPER_USER')")
    @ApiOperation(value = "Find user by userId")
    public UserDto getUserById(@PathVariable Long customerId,
                               @PathVariable Long userId) {
        return userService.getUserByIdAndCustomer(customerId, userId);
    }

    @GetMapping("/delete/{customerId}/{userId}")
    @PreAuthorize("hasRole('SUPER_USER')")
    @ApiOperation(value = "Delete user by userId")
    public void deleteUser(@PathVariable Long customerId,
                           @PathVariable Long userId) {
        userService.deleteUserByCustomer(customerId, userId);
    }

}
