package az.etaskify.controller;

import az.etaskify.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/assignment")
@Api("Task assigment controller")
public class AssigmentController {

    private final TaskService taskService;

    @GetMapping("/assignee/{userId}/{taskId}")
    @PreAuthorize("hasRole('SUPER_USER')")
    @ApiOperation(value = "Task assignee to user")
    public void assignTaskToUser(@PathVariable Long userId, @PathVariable Long taskId) {
        taskService.assignTaskToUser(taskId, userId);
    }

}



