package az.etaskify.service;

import az.etaskify.model.dto.UserDto;
import az.etaskify.model.request.CreateUserRequest;
import az.etaskify.model.response.MessageResponse;
import java.util.List;

public interface UserService {

    MessageResponse createUser(CreateUserRequest request);



    void deleteUserByCustomer(Long customerId, Long userId);

    UserDto getUserById(Long userId);

    UserDto getUserByIdAndCustomer(Long customerId, Long userId);

    List<UserDto> getUsersByCustomer(Long customerId);
}
