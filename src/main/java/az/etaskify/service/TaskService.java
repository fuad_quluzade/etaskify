package az.etaskify.service;

import az.etaskify.model.dto.TaskDto;

import java.util.List;

public interface TaskService {

    TaskDto createTask(TaskDto request);

    void deleteTaskByCustomer(Long customerId, Long id);

    List<TaskDto> findTasksByCustomer(Long customerId);

    void completedTask(Long id);

    void notCompletedTask(Long id);

    List<TaskDto> findFreeTasks(Long customerId);

    TaskDto getTaskByIdAndCustomer(Long customerId, Long taskId);

    void assignTaskToUser(Long taskId, Long userId);
}
