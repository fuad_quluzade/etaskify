package az.etaskify.service;

import az.etaskify.model.request.LoginRequest;
import az.etaskify.model.request.SignupRequest;
import az.etaskify.model.response.JwtResponse;
import az.etaskify.model.response.MessageResponse;
import org.springframework.web.bind.annotation.RequestBody;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

public interface AuthService {

    JwtResponse authenticateUser(LoginRequest loginRequest, HttpServletResponse response);

    MessageResponse registerUser(@Valid @RequestBody SignupRequest signUpRequest);
}
