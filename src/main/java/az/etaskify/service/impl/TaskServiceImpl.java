package az.etaskify.service.impl;

import az.etaskify.dao.Customer;
import az.etaskify.dao.Task;
import az.etaskify.exception.CustomerNotFoundException;
import az.etaskify.exception.TaskNotFoundException;
import az.etaskify.mapper.CustomerMapper;
import az.etaskify.mapper.TaskMapper;
import az.etaskify.mapper.UserMapper;
import az.etaskify.model.dto.TaskDto;
import az.etaskify.model.dto.UserDto;
import az.etaskify.repository.CustomerRepository;
import az.etaskify.repository.TaskRepository;
import az.etaskify.service.TaskService;
import az.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final CustomerRepository customerRepository;
    private final UserService userService;

    @Override
    public TaskDto createTask(TaskDto request) {
        Customer customer = customerRepository.findById(request.getCustomerDto().getId())
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        request.setCustomerDto(CustomerMapper.INSTANCE.customerEntityToDto(customer));
        Task task = taskRepository.save(TaskMapper.INSTANCE.taskDtoToEntity(request));
        return TaskMapper.INSTANCE.taskEntityToDto(task);
    }

    @Override
    public void deleteTaskByCustomer(Long customerId, Long id) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        taskRepository.deleteByIdAndCustomer(id, customer);
    }

    @Override
    public List<TaskDto> findTasksByCustomer(Long customerId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        List<TaskDto> tasks = taskRepository.findTasksByCustomer(customer).orElse(Collections.emptyList())
                .stream()
                .map(TaskMapper.INSTANCE::taskEntityToDto)
                .collect(Collectors.toList());
        return tasks;
    }

    @Override
    public void completedTask(Long id) {
        Task task = taskRepository.findById(id).orElseThrow(() -> {
            throw new TaskNotFoundException("Task not found");
        });
        task.setCompleted(true);
        taskRepository.save(task);
    }

    @Override
    public void notCompletedTask(Long id) {
        Task task = taskRepository.findById(id).orElseThrow(() -> {
            throw new TaskNotFoundException("Task not found");
        });
        task.setCompleted(false);
        taskRepository.save(task);
    }

    @Override
    public List<TaskDto> findFreeTasks(Long customerId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        return taskRepository.findTasksByCustomer(customer).orElse(Collections.emptyList())
                .stream()
                .filter(task -> task.getUsers() == null && !task.isCompleted())
                .map(TaskMapper.INSTANCE::taskEntityToDto).collect(Collectors.toList());
    }


    @Override
    public TaskDto getTaskByIdAndCustomer(Long customerId, Long taskId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        Task task = taskRepository.findByIdAndCustomer(taskId, customer).orElseThrow(() -> {
            throw new TaskNotFoundException("Task not found");
        });
        return TaskMapper.INSTANCE.taskEntityToDto(task);
    }

    @Override
    public void assignTaskToUser(Long taskId, Long userId) {
        Task task = taskRepository.findById(taskId).orElseThrow(() -> {
            throw new TaskNotFoundException("Task not found");
        });
        UserDto user = userService.getUserById(userId);
        if (task != null) {
            task.setUsers(Collections.singletonList(UserMapper.INSTANCE.userDtoToEntity(user)));
            taskRepository.save(task);
        }
    }
}
