package az.etaskify.service.impl;

import az.etaskify.config.security.jwt.JwtUtils;
import az.etaskify.config.security.services.UserDetailsImpl;
import az.etaskify.dao.Customer;
import az.etaskify.dao.Role;
import az.etaskify.dao.User;
import az.etaskify.exception.RoleNotFoundExceptions;
import az.etaskify.model.dto.CustomerDto;
import az.etaskify.model.request.LoginRequest;
import az.etaskify.model.request.SignupRequest;
import az.etaskify.model.response.JwtResponse;
import az.etaskify.model.response.MessageResponse;
import az.etaskify.repository.CustomerRepository;
import az.etaskify.repository.RoleRepository;
import az.etaskify.repository.UserRepository;
import az.etaskify.service.AuthService;
import az.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final RoleRepository roleRepository;
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final CustomerRepository customerRepository;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;
    private static final String ADMIN = "ROLE_SUPER_USER";

    @Override
    public JwtResponse authenticateUser(LoginRequest loginRequest, HttpServletResponse response) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        CustomerDto customerDto = userService.getUserById(userDetails.getId()).getCustomerDto();
        JwtResponse jwtResponse = JwtResponse.builder()
                .token(jwt)
                .id(userDetails.getId())
                .username(userDetails.getUsername())
                .email(userDetails.getEmail())
                .roles(roles)
                .build();
        return jwtResponse;
    }

    @Override
    public MessageResponse registerUser(@Valid SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new MessageResponse("Exists bu username");
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new MessageResponse("Exists by email");
        }

         Customer customer = Customer.builder()
                .name(signUpRequest.getCustomerName())
                .phoneNumber(signUpRequest.getCustomerPhoneNumber())
                .address(signUpRequest.getCustomerAddress())
                .build();
        customer = customerRepository.save(customer);
         User user = User.builder()
                .username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .name(signUpRequest.getName())
                .surname(signUpRequest.getSurname())
                .password(encoder.encode(signUpRequest.getPassword()))
                .customer(customer)
                .build();

        Role adminRole = roleRepository.findByName(ADMIN)
                .orElseThrow(() -> new RoleNotFoundExceptions("Role not found"));
        user.setRole(adminRole);
        userRepository.save(user);
        return new MessageResponse("Successfully registered");
    }
}

