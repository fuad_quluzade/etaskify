package az.etaskify.service.impl;

import az.etaskify.dao.Customer;
import az.etaskify.dao.User;
import az.etaskify.exception.CustomerNotFoundException;
import az.etaskify.exception.RoleNotFoundExceptions;
import az.etaskify.exception.UserNotFoundException;
import az.etaskify.mapper.UserMapper;
import az.etaskify.model.dto.UserDto;
import az.etaskify.model.request.CreateUserRequest;
import az.etaskify.model.response.MessageResponse;
import az.etaskify.repository.CustomerRepository;
import az.etaskify.repository.RoleRepository;
import az.etaskify.repository.UserRepository;
import az.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private static final String USER = "ROLE_USER";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final CustomerRepository customerRepository;
    private final PasswordEncoder encoder;

    @Override
    public MessageResponse createUser(CreateUserRequest request) {
        if (userRepository.existsByUsername(request.getUsername())) {
            return new MessageResponse("exists by username");
        }

        if (userRepository.existsByEmail(request.getEmail())) {
            return new MessageResponse("exists by email");
        }
        Customer customer = customerRepository.findById(request.getCustomerId())
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        User userEntity = UserMapper.INSTANCE.requestToEntity(request);
        userEntity.setCustomer(customer);
        userEntity.setRole(roleRepository.findByName(USER)
                .orElseThrow(() -> new RoleNotFoundExceptions("Role not found")));
        userEntity.setPassword(encoder.encode("DEFAULT_PASSWORD"));
        userEntity = userRepository.save(userEntity);
        UserDto user = UserMapper.INSTANCE.userEntityToDto(userEntity);
        return new MessageResponse("Successfully registered");
    }

    @Override
    public List<UserDto> getUsersByCustomer(Long customerId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        List<User> users = userRepository.findUsersByCustomer(customer)
                .orElseThrow(() -> new UserNotFoundException("Users not found"));
        return users.stream().map(UserMapper.INSTANCE::userEntityToDto).collect(Collectors.toList());
    }

    @Override
    public UserDto getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> {
            throw new UserNotFoundException("User not found");
        });
        return UserMapper.INSTANCE.userEntityToDto(user);
    }

    @Override
    public UserDto getUserByIdAndCustomer(Long customerId, Long userId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        User user = userRepository.findByIdAndCustomer(userId, customer).orElseThrow(() -> {
            throw new UserNotFoundException("User not found");
        });
        return UserMapper.INSTANCE.userEntityToDto(user);
    }

    @Override
    public void deleteUserByCustomer(Long customerId, Long id) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Customer not found"));
        User user = userRepository.findByIdAndCustomer(id, customer).orElseThrow(() -> {
            throw new UserNotFoundException("User not found");
        });
        userRepository.delete(user);
    }

}

