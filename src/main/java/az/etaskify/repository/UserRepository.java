package az.etaskify.repository;

import az.etaskify.dao.Customer;
import az.etaskify.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findByIdAndCustomer(Long id, Customer customer);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Optional<List<User>> findUsersByCustomer(Customer customer);
}
