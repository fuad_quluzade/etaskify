package az.etaskify.repository;

import az.etaskify.dao.Customer;
import az.etaskify.dao.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    Optional<List<Task>> findTasksByCustomer(Customer customer);

    void deleteByIdAndCustomer(Long id, Customer customer);

    Optional<Task> findByIdAndCustomer(Long id, Customer customer);
}
