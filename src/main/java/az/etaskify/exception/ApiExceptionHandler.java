package az.etaskify.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(CustomerNotFoundException.class )
    public ResponseEntity<Object> handleApiRequestExceptions(CustomerNotFoundException e){
        HttpStatus notFound=HttpStatus.NOT_FOUND;
          ApiException apiException = new ApiException(
                   e.getMessage(),
                   HttpStatus.NOT_FOUND,
                   ZonedDateTime.now()
           );
           return new ResponseEntity<>(apiException,notFound);
    }

    @ExceptionHandler(UserNotFoundException.class )
    public ResponseEntity<Object> handleApiRequestExceptions(UserNotFoundException e){
        HttpStatus notFound=HttpStatus.NOT_FOUND;
        ApiException apiException = new ApiException(
                e.getMessage(),
                HttpStatus.NOT_FOUND,
                ZonedDateTime.now()
        );
        return new ResponseEntity<>(apiException,notFound);
    }

    @ExceptionHandler(TaskNotFoundException.class )
    public ResponseEntity<Object> handleApiRequestExceptions(TaskNotFoundException e){
        HttpStatus notFound=HttpStatus.NOT_FOUND;
        ApiException apiException = new ApiException(
                e.getMessage(),
                HttpStatus.NOT_FOUND,
                ZonedDateTime.now()
        );
        return new ResponseEntity<>(apiException,notFound);
    }

    @ExceptionHandler(RoleNotFoundExceptions.class )
    public ResponseEntity<Object> handleApiRequestExceptions(RoleNotFoundExceptions e){
        HttpStatus notFound=HttpStatus.NOT_FOUND;
        ApiException apiException = new ApiException(
                e.getMessage(),
                HttpStatus.NOT_FOUND,
                ZonedDateTime.now()
        );
        return new ResponseEntity<>(apiException,notFound);
    }
}
