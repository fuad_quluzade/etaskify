package az.etaskify.exception;

public class RoleNotFoundExceptions extends RuntimeException{
    public RoleNotFoundExceptions(String message) {
        super(message);
    }
}
