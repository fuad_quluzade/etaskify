package az.etaskify.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskDto {

    private Long id;
    private String title;
    private String description;
    private LocalDate deadline;
    private boolean completed;
    private List<UserDto> users;
    private CustomerDto customerDto;
}
