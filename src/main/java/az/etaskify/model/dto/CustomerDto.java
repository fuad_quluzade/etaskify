package az.etaskify.model.dto;

import az.etaskify.dao.Task;
import az.etaskify.dao.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerDto {

    private Long id;
    private String name;
    private String phoneNumber;
    private String address;
    @JsonIgnore
    private Set<User> users;
    @JsonIgnore
    private Set<Task> tasks;
}


