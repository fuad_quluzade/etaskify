package az.etaskify.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignupRequest {

    private String username;
    private String name;
    private String surname;
    @Email
    private String email;
    @Size(min = 8)
    private String password;
    private String customerName;
    private String customerPhoneNumber;
    private String customerAddress;
}
